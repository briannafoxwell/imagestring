# ImageString
A simple way to convert 128x64 b/w images into strings e.g `x y state`

State is if the pixel is on or off

Requires PIL or PILLOW `pip3 install <pillow/pil>`